if __name__ == '__main__':

    import numpy as np
    import json
    from time import perf_counter
    from datetime import datetime

    from sklearn.datasets import fetch_openml
    from sklearn.model_selection import train_test_split
    from sklearn.ensemble import RandomForestClassifier as BRF

    from drf_module.my_drf import DynamicRandomForest as DRF

    # Import datasets
    dataset_A = fetch_openml('gas-drift', version=1)
    dataset_B = fetch_openml('isolet', version=1)
    dataset_C = fetch_openml('JapaneseVowels', version=1)
    dataset_D = fetch_openml('letter', version=1)
    dataset_E = fetch_openml('mfeat-factors', version=1)
    dataset_F = fetch_openml('musk', version=1)
    dataset_G = fetch_openml('optdigits', version=1)
    dataset_H = fetch_openml('page-blocks', version=1)
    dataset_I = fetch_openml('pendigits', version=1)
    dataset_J = fetch_openml('phoneme', version=1)
    dataset_K = fetch_openml('satimage', version=1)
    dataset_L = fetch_openml('segment', version=1)
    dataset_M = fetch_openml('spambase', version=1)
    dataset_N = fetch_openml('Speech', version=1)
    dataset_O = fetch_openml('steel-plates-fault', version=1)

    datasets = [dataset_A, dataset_B, dataset_C, dataset_D, dataset_E,
                dataset_F, dataset_G, dataset_H, dataset_I, dataset_J,
                dataset_K, dataset_L, dataset_M, dataset_N, dataset_O]

    # Set parameters
    n_tree = ['auto', 100, 200, 300]
    sliding_window = (20, .01)
    methods = ['DRF', 'BRF']
    max_features = 'sqrt'
    replications = 10

    parameters = {
        'n_tree': n_tree,
        'sliding_window': sliding_window,
        'methods': methods,
        'max_features': max_features,
        'replications': replications
    }

    # Import database
    # with open('my_results_database.json', 'r') as f:
    #     database = json.load(f)

    my_results = {'parameters': parameters}

    # Processing each dataset
    d = 0
    for data in datasets:
        dataset_name = data['details']['name']

        data_results = {'rep_'+str(rep): {} for rep in range(replications)}
        for rep in range(replications):
            print(dataset_name, '('+str(d)+'/'+str(len(datasets)-1)+') | Replication', str(rep)+'/'+str(replications-1))
            data_results['rep_'+str(rep)] = {
                'monitoring': {},
                'score': {},
                'time_fit': {},
                'time_score': {}
            }
            train_x, test_x, train_y, test_y = train_test_split(data.data, data.target, train_size=.66667)

            # Monitoring des courbes d'erreur OOB, DRF et BRF
            drf = DRF(n_tree=n_tree[-1], max_features=max_features)
            drf.fit(train_x, train_y)
            error_oob = drf._error_oob
            _, score_drf = drf.score(test_x, test_y, monitoring=True)

            brf = BRF(n_estimators=1, max_features=max_features, warm_start=True)
            score_brf = []
            for i in range(n_tree[-1]):
                brf.fit(train_x, train_y)
                brf.n_estimators += 1
                score_brf.append(brf.score(test_x, test_y))

            data_results['rep_'+str(rep)]['monitoring'] = {
                'OOB': error_oob,
                'DRF': (1 - np.asarray(score_drf)).tolist(),
                'BRF': (1 - np.asarray(score_brf)).tolist()
            }

            for n_t in n_tree:

                # Calculs de performance DRF
                drf = DRF(n_tree=n_t, max_features=max_features)

                start = perf_counter()
                drf.fit(train_x, train_y)
                stop = perf_counter()
                time_fit_drf = stop - start

                start = perf_counter()
                score_drf = drf.score(test_x, test_y)
                stop = perf_counter()
                time_score_drf = stop - start

                if n_t == 'auto':
                    n_auto = len(drf._error_oob)
                    data_results['rep_' + str(rep)]['n_auto'] = n_auto

                # Calculs de performance BRF
                brf = BRF(n_estimators=(n_t if n_t != 'auto' else n_auto), max_features=max_features)

                start = perf_counter()
                brf.fit(train_x, train_y)
                stop = perf_counter()
                time_fit_brf = stop - start

                start = perf_counter()
                score_brf = brf.score(test_x, test_y)
                stop = perf_counter()
                time_score_brf = stop - start

                data_results['rep_'+str(rep)]['score'][n_t] = {'DRF': score_drf, 'BRF': score_brf}
                data_results['rep_'+str(rep)]['time_fit'][n_t] = {'DRF': time_fit_drf, 'BRF': time_fit_brf}
                data_results['rep_'+str(rep)]['time_score'][n_t] = {'DRF': time_score_drf, 'BRF': time_score_brf}

        monitoring = np.zeros((replications, 3, n_tree[-1]))
        score = np.zeros((replications, len(n_tree), len(methods)))
        time_fit = np.zeros((replications, len(n_tree), len(methods)))
        time_score = np.zeros((replications, len(n_tree), len(methods)))
        for rep in range(replications):

            i = 0
            for error in ['OOB', 'DRF', 'BRF']:
                monitoring[rep][i] = data_results['rep_'+str(rep)]['monitoring'][error]
                i += 1

            i = 0
            for n_t in n_tree:
                score[rep][i] = tuple(data_results['rep_'+str(rep)]['score'][n_t].values())
                time_fit[rep][i] = tuple(data_results['rep_' + str(rep)]['time_fit'][n_t].values())
                time_score[rep][i] = tuple(data_results['rep_' + str(rep)]['time_score'][n_t].values())
                i += 1

        # Compute mean values
        data_results['mean'] = {
            'monitoring': {},
            'score': {},
            'time_fit': {},
            'time_score': {}
        }
        data_results['mean']['monitoring'] = {
            ['OOB', 'DRF', 'BRF'][i]: np.mean(monitoring, axis=0)[i].tolist() for i in range(3)
        }
        data_results['mean']['score'] = {
            n_tree[i]: {
                methods[j]: np.mean(score, axis=0)[i, j] for j in range(len(methods))
            } for i in range(len(n_tree))
        }
        data_results['mean']['time_fit'] = {
            n_tree[i]: {
                methods[j]: np.mean(time_fit, axis=0)[i, j] for j in range(len(methods))
            } for i in range(len(n_tree))
        }
        data_results['mean']['time_score'] = {
            n_tree[i]: {
                methods[j]: np.mean(time_score, axis=0)[i, j] for j in range(len(methods))
            } for i in range(len(n_tree))
        }

        # Compute standard deviation values
        data_results['std'] = {
            'monitoring': {},
            'score': {},
            'time_fit': {},
            'time_score': {}
        }
        data_results['std']['monitoring'] = {
            ['OOB', 'DRF', 'BRF'][i]: np.std(monitoring, axis=0)[i].tolist() for i in range(3)
        }
        data_results['std']['score'] = {
            n_tree[i]: {
                methods[j]: np.std(score, axis=0)[i, j] for j in range(len(methods))
            } for i in range(len(n_tree))
        }
        data_results['std']['time_fit'] = {
            n_tree[i]: {
                methods[j]: np.std(time_fit, axis=0)[i, j] for j in range(len(methods))
            } for i in range(len(n_tree))
        }
        data_results['std']['time_score'] = {
            n_tree[i]: {
                methods[j]: np.std(time_score, axis=0)[i, j] for j in range(len(methods))
            } for i in range(len(n_tree))
        }

        # Add dataset results to my results
        my_results[dataset_name] = data_results
        d += 1

        date_time = datetime.now().strftime('%Y%m%d_%H%M%S')
        my_results_json = json.dumps(data_results)
        f = open('./output/my_results_' + dataset_name + '.json', 'w')
        f.write(my_results_json)
        f.close()

    date_time = datetime.now().strftime('%Y%m%d_%H%M%S')
    my_results_json = json.dumps(my_results)
    f = open('./output/my_database_' + date_time + '.json', 'w')
    f.write(my_results_json)
    f.close()
