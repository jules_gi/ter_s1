"""Module permettant de développer des fonctions utilitaires pour la librairie."""

# Authors : ...

import numpy as np
import random


def weighted_sample(n_samples, weights):
    """Fonction permettant de réaliser un bootstrap pondéré et associé son out-of-bag."""
    # Trouver une autre solution à random.choices, car ne fonctionne pas bien avec seed().
    bootstrap = np.asarray(random.choices(range(n_samples), weights=weights, k=n_samples)) # Bootstrap instances
    oob = np.asarray(range(n_samples))[np.isin(range(n_samples), bootstrap, invert=True)] # OOB instances
    
    return bootstrap, oob


def labels_to_dict(y):
    """
    Fonction permettant de convertir une liste de labels en dictionnaire (keys=labels, values=unique_integer)
    """
    labels = {} ; i=0
    for label in np.unique(y):
        labels[label] = i
        i += 1
    return labels, len(labels)