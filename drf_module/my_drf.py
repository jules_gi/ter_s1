"""Module implémentant les forêts aléatoires (method dynamicRF pour le moment)"""

# Authors : ...

import numpy as np
from operator import itemgetter
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

from .utils import weighted_sample
from .utils import labels_to_dict


count_votes = lambda votes: np.unique(votes, return_counts=True)
# Je ne pense pas que ce soit correct de déclarer ça comme ça et ici (car je ne l'ai jamais vu autre part)
# Mais c'est la solution que j'ai trouvé pour compter efficacement les votes sans utiliser de boucle
# (Je me pose quand même la question de savoir si lambda utilise une boucle ou non ??)

# ---------------------------- #
# Dynamic Random Forest method #
# ---------------------------- #


class DynamicRandomForest:
    
    def __init__(self,
                 n_tree='auto',
                 max_features='sqrt',
                 sliding_window='default'):
        self.n_tree = n_tree
        self.max_features = max_features
        self.forest = None
        self.random_state = random_state
        
        if n_tree == 'auto':
            if sliding_window == 'default':
                self.sliding_window = (20, .01)
            else:
                if isinstance(sliding_window, tuple):
                    if len(sliding_window) == 2 and isinstance(sliding_window[0], int) and isinstance(sliding_window[1], float):
                        if sliding_window[1] >= 0:
                            self.sliding_window = sliding_window
                        else:
                            raise ValueError('slinding_window must be >= 0.')
                    else:
                        raise TypeError('sliding_window must be length 2, containing an integer and a float.')
                else:
                    raise TypeError("sliding_window must be a tuple of length 2.")
        else:
            if isinstance(n_tree, int):
                self.n_tree = n_tree
                self.sliding_window = (0, 0)
            else:
                raise TypeError("n_tree must be an integer.")

    def fit(self, X, y):
        """Build a decision tree from the training set (X, y), weighted by samples_weight.
        
        Parameters
        ''''''''''
        X : The training input samples
        (n_samples * n_features)
        
        y : The target values
        (n_samples)
        
        samples_weight : weights
        """
        
        n_samples, n_features = X.shape
        self._labels, self._n_labels = labels_to_dict(y)
        y_values = np.array(itemgetter(*y)(self._labels))
        D = np.repeat(1/n_samples, n_samples)
        votes_w = np.zeros((n_samples, self._n_labels), int)
        votes_oob = np.copy(votes_w)
        instances_oob = np.argwhere(np.sum(votes_oob, axis=1) != 0).T[0]
        self._error_oob = []
        self.forest = []

        critere = 1.
        self._critere_list = []

        i = 0
        while critere > self.sliding_window[1]:

            # Weighted sampling
            bootstrap, oob = weighted_sample(n_samples, D)
            
            # Add a tree to forest
            tree = DecisionTreeClassifier(max_features=self.max_features).fit(X[bootstrap], y[bootstrap])
            self.forest.append(tree)
            i += 1

            # Prediction of this new tree
            predict = np.array(itemgetter(*tree.predict(X))(self._labels))
            votes_w[range(n_samples), predict] += 1
            votes_oob[oob, predict[oob]] += 1

            if len(instances_oob) != n_samples:
                instances_oob = np.argwhere(np.sum(votes_oob, axis=1) != 0).T[0]
            # On calcul le poids dès le début, sinon notre erreur OOB reste stable
            # et notre critère d'arrêt est atteint dès le début
            D = 1 - (votes_w[range(n_samples), y_values] / np.sum(votes_w, axis=1))
            D = D/np.sum(D)
            
            # Calcul de l'erreur OOB
            self._error_oob.append(np.mean(1 - (votes_oob[instances_oob, y_values[instances_oob]] / np.sum(votes_oob[instances_oob], axis=1))))

            # Critère d'arret | Sliding window
            if self.n_tree == 'auto' and i >= self.sliding_window[0]+np.argmax(self._error_oob):
                self._critere_list.append(-np.sum(np.diff(self._error_oob[-self.sliding_window[0]:])))
                critere = self._critere_list[-1]
            
            # Critère d'arret | Nombre d'arbres
            elif i == self.n_tree:
                break

        self.n_tree = i

    def predict_proba(self, X):
        """Calcul les probabilités d'appartenir à un label pour chaque données"""

        self.n_output = X.shape[0]
        if self.forest is not None:
            votes = np.asarray([tree.predict(X) for tree in self.forest]).T
            votes = list(map(count_votes, votes))

            proba = np.zeros((self.n_output, self._n_labels))
            for i in range(self.n_output):
                proba[i, itemgetter(*votes[i][0])(self._labels)] = votes[i][1]

            proba /= self.n_tree
        else:
            raise ValueError("Il faut fitter les données avant de prédire.")
        
        return proba

    def predict(self, X):
        """Prédit le label d'un ensemble de données test"""

        if self.forest is not None:
            proba = self.predict_proba(X)
            votes = np.asarray([*self._labels.keys()])[np.argmax(proba, axis=1)]
        else:
            raise ValueError("Il faut fitter les données avant de prédire.")
        
        return votes

    def score(self, X, y, monitoring=False):
        """Calcul le taux de bonne classification"""
        self.n_output = X.shape[0]
        if monitoring:
            score_list = []
            for i in np.arange(1, self.n_tree+1):
                votes = np.asarray([tree.predict(X) for tree in self.forest[:i]]).T
                votes = list(map(count_votes, votes))

                proba = np.zeros((self.n_output, self._n_labels))
                for i in range(self.n_output):
                    proba[i, itemgetter(*votes[i][0])(self._labels)] = votes[i][1]

                proba /= i

                predict = np.asarray([*self._labels.keys()])[np.argmax(proba, axis=1)]
                score_list.append(np.sum(y == predict)/self.n_output)
            score = score_list[-1]
            
            return score, score_list

        else:
            predict = self.predict(X)
            score = np.sum(y == predict)/self.n_output

            return score


if __name__ == '__main__':
    
    from sklearn.datasets import fetch_openml
    from sklearn.model_selection import train_test_split

    dataset = fetch_openml('letters', version=1)
    train_x, test_x, train_y, test_y = train_test_split(dataset.data, dataset.target, train_size=.67)

    test = DynamicRandomForest(n_tree = 50)
    forest = test.fit(train_x, train_y)
    proba = test.predict_proba(test_x)
    votes = test.predict(test_x)
    
    for i in range(10):
        print('Donnée '+str(i)+' | Probabilités : '+str(proba[i])+' -> '+str(votes[i]))
    
    print('Taux de bonne classification : '+str(test.score(test_x, test_y)))