# Master Degree Project - Semester 1

This project is about the method [Dynamic Random Forest](https://www.researchgate.net/publication/257015054_Dynamic_Random_Forests) (DRF).

The aim of the study is to analyse the behaviour of DRF and make a comparison with Forest-RI algorithm.

It contains the followings files and directories :
- [DRF_module](drf_module/): python implementation of the method
- [benchmark.py](benchmark.py): script used to run the experiments
- [Resultats.json](Resultats.json): store the results of the experiments
